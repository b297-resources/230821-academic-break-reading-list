# 230821 - Batch 297:  Academic Reading List 

### **Topics**

- Continuation of Capstone 2 Making
  - Additional resources
    - [How I built an E-commerce API with NodeJs, Express and MongoDB(Part 1)](https://medium.com/geekculture/how-i-built-an-e-commerce-api-with-nodejs-express-and-mongodb-7b42b5253ffb)
    - [How I built an E-commerce API with NodeJs, Express and MongoDB(Part 2)](https://medium.com/geekculture/how-i-built-an-e-commerce-api-with-nodejs-express-and-mongodb-part-2-c729b1e74336)
    - [How I built an E-commerce API with NodeJs, Express and MongoDB(Part 3)](https://medium.com/geekculture/how-i-built-an-e-commerce-api-with-nodejs-express-and-mongodb-part-3-60150d354587)
    - [How I built an E-commerce API with NodeJs, Express and MongoDB(Part 4)](https://medium.com/geekculture/how-i-built-an-e-commerce-api-with-nodejs-express-and-mongodb-part-4-318e3f494611)


### **Purpose**
- To be able to add and implement more functionalities and features to my eCommerce API.
- To have time to debug my code if bugs may arise during developement.


### **Goal to Checking**

#### In your Academic Break Reading List Folder, create a folder named 230821:

1. Add the following features and functionalities to your eCommerce API:
    - Non-admin User checkout (Create Order)
    - Retrieve User Details
    - Stretch Goals:
        - Set user as admin (Admin only)
        - Retrieve authenticated user’s orders
        - Retrieve all orders (Admin only)
        - Add to Cart
            - Added Products
            - Change product quantities
            - Remove products from cart
            - Subtotal for each item
            - Total price for all item
    - More unique functionalities you want to add (Only if the tasks outlined in the above deliverables have been completed.)

2. Add a screen cap for each feature's route in Postman with the following folder structure:<br>

        - 230821
            - routeName1 
                - sreencap1.png
                - sreencap2.png
                - ...
            - routeName2 
                - sreencap1.png
                - sreencap2.png
                - ...
            - ...


    


